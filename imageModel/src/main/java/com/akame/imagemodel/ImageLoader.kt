package com.akame.imagemodel

import android.widget.ImageView
import com.bumptech.glide.Glide

class ImageLoader {
    fun loadImage(imageView: ImageView, path: String) {
        Glide.with(imageView.context)
            .load(path)
            .into(imageView)
    }
}