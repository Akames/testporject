package com.akame.releaseporject

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import com.akame.imagemodel.ImageLoader

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val imageView = findViewById<ImageView>(R.id.ivImage)
        val imagePath = "https://www.liaoxuefeng.com/files/attachments/1367702604087361/0"
        ImageLoader().loadImage(imageView, imagePath)
    }
}